from app import app
from flask import render_template

@app.route("/galeri")
def galeri():
    return render_template("galeri.html")